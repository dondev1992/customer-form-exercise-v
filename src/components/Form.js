import classes from "./Form.module.css";

const Form = ({ formErrors, form, handleSubmit, handleInput }) => {
  const errorStyle = {
    fontWeight: "bold",
    fontStyle: "italic",
    color: "red",
    padding: "5px",
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className={classes.field}>
        <label htmlFor="name">Name</label>
        <input
          type="text"
          name="name"
          onChange={handleInput}
          value={form.name}
          style={{
            border: formErrors.name ? "2px solid red" : "2px solid black",
          }}
        />
        <p style={errorStyle}>{formErrors.name}</p>
      </div>
      <div className={classes.field}>
        <label htmlFor="email">Email</label>
        <input
          type="text"
          name="email"
          onChange={handleInput}
          value={form.email}
          style={{
            border: formErrors.email ? "2px solid red" : "2px solid black",
          }}
        />
        <p style={errorStyle}>{formErrors.email}</p>
      </div>
      <div className={classes.field}>
        <label htmlFor="zipcode">Zip Code</label>
        <input
          type="text"
          name="zipcode"
          onChange={handleInput}
          value={form.zipcode}
          style={{
            border: formErrors.zipcode ? "2px solid red" : "2px solid black",
          }}
        />
        <p style={errorStyle}>{formErrors.zipcode}</p>
      </div>
      <button className={classes.formButton}>SUBMIT</button>
    </form>
  );
};

export default Form;
