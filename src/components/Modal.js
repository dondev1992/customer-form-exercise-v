import classes from "./Modal.module.css";

const Modal = ({ closeModal, openModal }) => {
  if (!openModal) {
    // console.log("not open")
    return null;
  }

  return (
    <div className={classes.background}>
      <div className={classes.modal}>
        <h3>Form was Submitted Successfully!</h3>
        <button className={classes.button} onClick={closeModal}>
          Close
        </button>
      </div>
    </div>
  );
};

export default Modal;
