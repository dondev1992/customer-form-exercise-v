import { useState, useEffect } from "react";
import "./App.css";
import Form from "./components/Form";
import Modal from "./components/Modal";
import Card from "./components/Card";

function App() {
  const [form, formSet] = useState({
    name: "",
    email: "",
    zipcode: "",
  });
  const [showModal, showModalSet] = useState(false);
  const [formErrors, formErrorsSet] = useState({});
  const [isSubmitted, isSubmittedSet] = useState(false);

  // const errorStyle = { border: formErrors.name ? "1px solid red" : null, fontStyle: "italic", color: "red" }

  useEffect(() => {
    // console.log(formErrors);
    if (Object.keys(formErrors).length === 0 && isSubmitted) {
      showModalSet(true);
      formSet({
        name: "",
        email: "",
        zipcode: "",
      });
      // console.log(form);
    }
  }, [formErrors]);

  const handleInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    formSet((prev) => {
      return { ...prev, [name]: value };
    });
    // console.log(form)
  };

  const validateForm = (values) => {
    const errors = {};
    const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    const regexZipcode = /^\d{5}(-\d{4})?$/;
    if (!values.name) {
      errors.name = "Please enter a name";
    } else if (values.name.length < 3) {
      errors.name = "Must be at least 3 characters";
    }
    if (!values.email) {
      errors.email = "Please enter an email";
    } else if (!regexEmail.test(values.email)) {
      errors.email = "Must be a valid email";
    }
    if (!values.zipcode) {
      errors.zipcode = "Please enter a zipcode";
    } else if (!regexZipcode.test(values.zipcode)) {
      errors.zipcode = "Must be a valid zip code";
    }
    return errors;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    formErrorsSet(validateForm(form));
    isSubmittedSet(true);
  };

  return (
    <div className="App">
      <Modal openModal={showModal} closeModal={() => showModalSet()} />
      <Card>
        <h1>Customer Form</h1>
        <Form
          formErrors={formErrors}
          form={form}
          handleSubmit={handleSubmit}
          handleInput={handleInput}
        />
      </Card>
    </div>
  );
}

export default App;
